#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>

double f(double x){
	return 4 / (1 + x * x);
	//return 1.;
}
double begin, end, total;

int main(int argc, char*argv[]){
        int i, n, m;
	n = 10;
	double h = 1. / n;
	int gran1;
	int gran2;	
        int myrank,size;
        MPI_Init(&argc, &argv);
	MPI_Status Status;	
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	double summ[size];
	m = n % size;
	n = n - m;
	int k = 0;
	if(myrank == 0){

		begin = MPI_Wtime();

		if (m != 0){
			k += 1;
		}

 		summ[myrank] = 0.;
	
		for (i = 1; i < size; i++){
			gran1 = i * n / size + k;
			if (k < m){
				gran2 = (i + 1) * n / size + 1 + k;
				k += 1;
			}
			else{
				gran2 = (i + 1) * n / size + k;
			}	
			MPI_Send(&gran1, 1, MPI_INT, i, i, MPI_COMM_WORLD);
			MPI_Send(&gran2, 1, MPI_INT, i, i, MPI_COMM_WORLD);
			//printf("Gran1 %d \n ",gran1);
			//printf("Gran2 %d \n", gran2);
		}
		if (m != 0){
			k = 1;
		}
		for (i = 0; i < n / size + k; i++){
			summ[myrank] += f(i * h) * 0.5 * h + f((i + 1) * h) * 0.5 * h;
			printf("i = %d \n", i);
		}

		printf("\n");
		printf("Summa %d = ", myrank); 
		printf("%.12f \n ", summ[myrank]);


	}

	if (myrank != 0){

		MPI_Recv(&gran1, 1, MPI_INT, 0, myrank, MPI_COMM_WORLD, &Status);
		MPI_Recv(&gran2, 1, MPI_INT, 0, myrank, MPI_COMM_WORLD, &Status);

		summ[myrank] = 0.;

		for (i = gran1; i < gran2; i++){
			summ[myrank] += (f(i * h) + f((i + 1) * h)) * 0.5 * h;
			printf("i= %d \n", i);
		}

		printf("Summa %d = ", myrank);
		printf("%f \n", summ[myrank]); 	

		MPI_Send(&summ[myrank],1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
		printf("\n");
	}
	
	if (myrank == 0){
		for (i = 1; i < size; i++){
			MPI_Recv(&summ[i], 1, MPI_DOUBLE, i, myrank, MPI_COMM_WORLD, &Status);
		}
		double summa = 0;
		for (i = 0; i < size; i++){
			summa += summ[i];
		} 
		printf("Integral %.12f \n ", summa);
		printf("Time %.8f \n", MPI_Wtime() - begin); 
	}
        MPI_Finalize();
 

       return 0;
}

