#!/bin/bash
#
#SBATCH --ntasks=1
#SBATCH --partition=RT
#SBATCH --cpus-per-task=1
#SBATCH --job-name=main
#SBATCH --comment "sem_gr_m01-901"
module load mpi/openmpi4-x86_64
mpicc Art.c
mpiexec ./a.out
