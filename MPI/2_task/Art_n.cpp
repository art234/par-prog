#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>
//#include<math.h>
#include<cmath>
double pi = 3.14159265;
double f(double x, double T){

	double summ = 0;
	int m;
	double u;
	for (m = 0; m < 10000; m++){
		u = 1;
		u *= exp(-pi * pi * (2 * m + 1) * (2 * m + 1) * T);
		u /= 2 * m + 1;
		u *= sin (pi * (2 * m + 1) * x);
		summ += u;
	}

	summ *= 4 / pi;
	return summ;
}	
double begin, end, total;

int main(int argc, char*argv[]){
	printf("%f \n",f(0.2, 0.2) );
    int i, n, m;
	
	double k = 1.;
	double u0 = 1.;


	/*double h = 0.02;
	n = int(1 / h) + 1;	
	double dt = 0.0002;
	double C = dt * k / h / h;*/


	double T = 0.001;


	double C = 0.5 ;
	n = 2000;
	double h = 1. / (n - 1);
	double dt = h * h / C / k;

        int myrank,size;

    MPI_Init(&argc, &argv);
	MPI_Status Status;	
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	m = n % size;
	n = n - m;
	int t;
	if (myrank == 0){
		begin = MPI_Wtime();
	}
	if (myrank < m)
		t = n / size + 1 + 2;
	else
		t = n / size + 2;

	double array[t];
	double new_array[t];
	array[t - 1] = 0.;
	array[0] = 0.;
	for(i = 1; i < t - 1; ++i){
		array[i] = u0;
	}
	if (myrank == 0)
		array[1] = 0.;
	if (myrank == size - 1)
		array[t - 2] = 0.;

	int j;
	
	double tim = 0;

	while (tim < T){
		
		if (myrank != size - 1){
			MPI_Send(&array[t - 2], 1, MPI_DOUBLE, myrank + 1, myrank + 1, MPI_COMM_WORLD );
			MPI_Recv(&array[t - 1], 1, MPI_DOUBLE, myrank + 1, myrank,  MPI_COMM_WORLD, &Status);
		}

		if (myrank != 0){
			MPI_Recv(&array[0], 1, MPI_DOUBLE, myrank - 1, myrank,  MPI_COMM_WORLD, &Status);
			MPI_Send(&array[1], 1, MPI_DOUBLE, myrank - 1, myrank - 1, MPI_COMM_WORLD );
		}

		for (i = 1; i < t - 1; ++i){
			new_array[i] = array[i] + C * (array[i + 1] - 2 * array[i] + array[i - 1]);
		}

		for (i = 1; i < t - 1; ++i){
			array[i] = new_array[i];
		}

		if(myrank == 0)
			array[1] = 0.;
	        if(myrank == size - 1)
			array[t - 2] = 0.;	
		tim += dt;
	}	

	/*for (j = 0; j < size; ++j){
	       if (myrank == j){
	       		printf("Myrank =  %d :", myrank);
	 		for (i = 1; i < t - 1; ++i){
				printf(" %f ", array[i]);
			}
			printf("\n ");
		}
	}*/
	if (myrank == 0){	
		printf("Time %.8f \n", MPI_Wtime() - begin); 
	}

	//printf("Tochnoe resh \n");

	for(i = 0; i < n ; i++){
		//printf(" %f ", f(i * h, T) );
	}

	printf("\n");	
		
        MPI_Finalize();
 

       return 0;
}

